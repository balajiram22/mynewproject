package bI.framework.project;

import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class ZoomCar extends SeleniumBase {
		
		String str = "? 2520";
		
	
	@Test
	public void login() {
		startApp("Chrome", "https://www.zoomcar.com/chennai");
		WebElement searchJourney = locateElement("xpath","//a[text()='Start your wonderful journey']");
		click(searchJourney);
		WebElement chooseLocation = locateElement("xpath","//div[text()='Popular Pick-up points']/following-sibling::div[2]");
		click(chooseLocation);
		WebElement nextButton = locateElement("xpath","//button[text()='Next']");
		click(nextButton);
		WebElement pickTime = locateElement("xpath","//div[text()='Mon']");
		click(pickTime);
		WebElement nextButton2 = locateElement("xpath","//button[text()='Next']");
		click(nextButton2);
		WebElement dropTime = locateElement("xpath","//div[text()='Tue']");
		click(dropTime);
		WebElement doneProcess = locateElement("xpath","//button[text()='Done']");
		click(doneProcess);
		List<WebElement> cars = driver.findElementsByXPath("//div[@class='price']");
		System.out.println(cars.size());
		for (WebElement carPrice : cars) {
			System.out.println(carPrice.getText());
			String replace = str.replaceAll("\\D", " ");
			System.out.println(replace);
		}
		//List<WebElement> carsmax = driver.findElementsByXPath("//div[@class='price']");
		//System.out.println("Maximum price from the list: " + Collections.max(carsmax));
	}


}
