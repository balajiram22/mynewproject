package bl.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends ProjectMethods {
	/*@BeforeMethod(groups = {"sanity"})
	public void login() {
		WebElement create = locateElement("id","Create Lead");
		click(create);
	}
	*/
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
		dataSheetName = "TC001";
	} 
	@Test(dataProvider="fetchData")
	public void createLead(String cname, String fname, String lname) {
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), cname);
		clearAndType(locateElement("id", "createLeadForm_firstName"),fname);
		clearAndType(locateElement("id", "createLeadForm_lastName"), lname);
		click(locateElement("name", "submitButton")); 
	}
	
	@DataProvider(name = "getdata")
	public String[][] fetchdata(){
		String[][] data = new String[2][3];
		data[0][0] = "Testleaf";
		data[0][1] = "Sarath";
		data[0][2] = "C";
		data[1][0] = "TestLeaf";
		data[1][1] = "Gopi";
		data[1][2] = "J";
		return data;
	}	
	
	@DataProvider(name = "getdata2")	
	public String[][] resultdata(){
		String[][] datas = new String[2][3];
		datas[0][0] = "Testleaf";
		datas[0][1] = "Jai";
		datas[0][2] = "C";
		datas[1][0] = "TestLeaf";
		datas[1][1] = "Vinoth";
		datas[1][2] = "J";
		return datas;
		
		
	}
}
