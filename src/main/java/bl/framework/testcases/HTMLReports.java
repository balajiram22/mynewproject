package bl.framework.testcases;

import java.io.IOException;

import org.testng.annotations.Test;

//import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HTMLReports {
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	@Test
	public void runReports() {
		html = new ExtentHtmlReporter("./reports/extentReport2.html");
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test = extent.createTest("TC001_Login","Login the LeafTaps");
		test.assignAuthor("Surendar");
		//test.assignCategory("Sanity");
		//test.pass("UserName DemoSalesManage entered successfully...");
		try {
			test.fail("Username DemoSalesManage entered successfully..",
					MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());

		} catch (IOException e) {
			e.printStackTrace();
		}
		extent.flush();
	}

}
